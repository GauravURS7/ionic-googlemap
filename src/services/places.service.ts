import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PlaceService {

  constructor( ){
    console.log('PlaceService.....');
  }
  

    lat : 0;
    lng : 0;
  
 markers = [
	  {
		  lat: 51.673858,
		  lng: 7.815982,
          placeName: '',
		  label: 'A',
		  draggable: true
	  },
	  {
		  lat: 51.373858,
		  lng: 7.215982,
          placeName: '',
		  label: 'B',
		  draggable: false
	  },
	  {
		  lat: 51.723858,
		  lng: 7.895982,
          placeName: '',
		  label: 'C',
		  draggable: true
	  }
  ]
  
  j:any;
  

setPosition(lat,lng) : any {
    
        this.lat=lat;
        this.lng = lng;
    
}

getPlaceViaName(name:string) {  
    
    for(this.j in this.markers){
        
            if(name === this.markers[this.j].placeName){
                return this.markers[this.j];
            }
    }
    
}

getLat() : any {
    return this.lat;
}

getLng() : any {
    return this.lng;
}

  getMarkers() : any {
     return this.markers;
  }
  
  setMarkers(newPlace:any) : void {
    this.markers.push(newPlace);
  }

}
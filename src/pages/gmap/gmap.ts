import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { PlaceService } from '../../services/places.service';

declare var google;
 
@Component({
    selector : 'page-gmap',
    templateUrl : 'gmap.html'
})
export class GmapPage {
 
 placeName : string;
 lat : number;
 lng : number;
 
  @ViewChild('map') mapElement: ElementRef;
  map: any;
 
  constructor(public navCtrl: NavController,private placeService:PlaceService, public alertCtrl : AlertController ) {
  
    this.lng = 77.594563;
    this.lat = 12.971599;
 
  }
 
  ionViewDidLoad(){
    this.loadMap();
  }
 
  loadMap(){
 
    let latLng = new google.maps.LatLng(this.lat, this.lng);
 
    let mapOptions = {
      center: latLng,
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
 
  }//loadMap()

//search places via name
   findPlace(){
     
     if(this.placeService.getPlaceViaName(this.placeName)){
        this.lat = this.placeService.getPlaceViaName(this.placeName).lat;
        this.lng =this.placeService.getPlaceViaName(this.placeName).lng; 
        let latLng = new google.maps.LatLng(this.lat, this.lng);
              
                    let myOptions = {
                        zoom: 10,
                        center: latLng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
              
                    this.map = new google.maps.Map(document.getElementById("map"),myOptions);
                    
                    let marker = new google.maps.Marker({
                        map: this.map,
                        animation: google.maps.Animation.DROP,
                        position: latLng
                    });
 
                    let content = "<h4>Information!</h4>";          
 
                    let infoWindow = new google.maps.InfoWindow({
                        content: content
                    });
 
                    google.maps.event.addListener(marker, 'click', () => {
                        infoWindow.open(this.map, marker);
                    });
     }
     else {
     let geocoder = new google.maps.Geocoder();
     
     geocoder.geocode({
            'address': this.placeName
          }, function(results, status) {
                
                if (status === google.maps.GeocoderStatus.OK) {
            
                    this.lat = results[0].geometry.location.lat();
                    this.lng = results[0].geometry.location.lng();         
  
                    let latLng = new google.maps.LatLng(this.lat, this.lng);
              
                    let myOptions = {
                        zoom: 10,
                        center: latLng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
              
                    this.map = new google.maps.Map(document.getElementById("map"),myOptions);
                    
                    let marker = new google.maps.Marker({
                        map: this.map,
                        animation: google.maps.Animation.DROP,
                        position: latLng
                    });
 
                    let content = "<h4>Information!</h4>";          
 
                    let infoWindow = new google.maps.InfoWindow({
                        content: content
                    });
 
                    google.maps.event.addListener(marker, 'click', () => {
                        infoWindow.open(this.map, marker);
                    });
                    
                } else {
                        alert("Something went wrong. ");
                  }
            }//function(-,-)
        );//geocoder
    }//else
    
  }//findplace()
 
 //add new place
 newPlace(){
    
 }
}
import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { PlaceService } from '../../services/places.service';
import { AboutPage } from '../../pages/about/about';

@Component({
  selector: 'page-addNewPlace',
  templateUrl: 'addNewPlace.html'
})

export class AddNewPlacePage {

  constructor(public navCtrl: NavController, private placeService: PlaceService,public loadingCtrl: LoadingController) {

  }
  marker : Marker;
  newPlaceName: string;
  newPlaceLat = this.placeService.getLat();
  newPlaceLng = this.placeService.getLng();
  
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
  }
  
  cancel(){
  this.navCtrl.push(AboutPage);
  }
  
  saveNewPlace(){
    this.marker = {
    lat:this.newPlaceLat,
     lng: this.newPlaceLng,
	 placeName: this.newPlaceName,
     label:'',
     draggable: false
    }
    
    this.placeService.setMarkers(this.marker);
    
    console.log("new place is saved");
    
    this.presentLoading();
    
    this.navCtrl.push(AboutPage);
  }
  
}

interface Marker {
     lat:number,
     lng: number,
     placeName:any,
	 label: any,
     draggable: boolean
}
import { Component } from '@angular/core';
import { NavController, PopoverController, ToastController, LoadingController } from 'ionic-angular';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { AddNewPlacePage } from '../addNewPlace/addNewPlace';
import { PlaceService } from '../../services/places.service';

declare var google;

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public popoverCtrl: PopoverController,public navCtrl: NavController,private placeService:PlaceService,public toastCtrl: ToastController,public loadingCtrl: LoadingController) {

  }
  //bind the new place name
  newPlaceName : string;
  
  // google maps zoom level
  zoom: number = 8;
  mark : Mark;
  // initial center position for the map
  lat: number = 51.673858;
  lng: number = 7.815982;
  pos:string = "middle";
  
  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`); 
  }
  
  // show the marker when event click occurs
  mapClicked($event: any) {
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable:true
    });
    
    this.placeService.setPosition($event.coords.lat,$event.coords.lng);
    
    this.navCtrl.push(AddNewPlacePage);
    
    //let popover = this.popoverCtrl.create(AddNewPlacePage);
    //popover.present();
  }
  
  
  // adding missing place
  addPlace() {
  console.log(this.placeService.getMarkers());
  
   let toast = this.toastCtrl.create({
      message: 'Mark the location on map* first',
      position: this.pos,
      duration: 3000
    });
    toast.present();
   
   //this.navCtrl.push(AddNewPlacePage);
  }
  
  //search place via name
  searchPlace() {
  console.log(this.placeService.getPlaceViaName(this.newPlaceName));
    if(this.placeService.getPlaceViaName(this.newPlaceName)){
        this.lat = this.placeService.getPlaceViaName(this.newPlaceName).lat;
        this.lng =this.placeService.getPlaceViaName(this.newPlaceName).lng; 
        this.markers.push(this.placeService.getPlaceViaName(this.newPlaceName));
    }
    else {
    console.log("kuch hua"+this.newPlaceName);
    let geocoder = new google.maps.Geocoder();
     
     geocoder.geocode({
            'address': this.newPlaceName
          }, function(results, status) {
                
                if (status === google.maps.GeocoderStatus.OK) {
            
                    this.lat = results[0].geometry.location.lat();
                    this.lng = results[0].geometry.location.lng();     
                    console.log(this.lng+" kuch hua2 "+this.lat);
                    console.log(results);
                   
                    this.mark = {
                        lat: this.lat,
                        lng: this.lng,
	                    placeName: results[0].address_components[0].long_name,
                        label:'',
                        draggable: false
                    }
                    console.log(this.mark);
                    
                    //this.placeService.setMarkers(this.mark);
                    
                    this.markers = this.mark; 
                    console.log(this.markers);
                    //.push(this.placeService.getPlaceViaName(this.newPlaceName));
                    
                    
  
                } else {
                        alert("Something went wrong. ");
                  }
            }//function(-,-)
    );//geocoder

    }
}
  
  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }
  
  markers: marker[] = this.placeService.getMarkers();
  
}

// just an interface for type safety.
interface marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}

interface Mark {
     lat:number,
     lng: number,
     placeName:any,
	 label: any,
     draggable: boolean
}